<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreatePostRequest;

class CategoryController extends Controller
{
    
    public function getCategory(){

    	$result = DB::select('select * from categoria_novedades');
    	foreach ($result as $key => $grafi) $s[] = $grafi;                            
        return $s;   
    }

    public function getCountChart(Request $request){
        $request = $request->all();

        if($request['rol'] != 1) $aux = "="; 
        else $aux = "<>";

        if($request['idSubCat'] == 0)
          $result = DB::select('select sub_categoria_novedades.id, sub_categoria_novedades.nombre, count(*) as cantidad from novedades inner join sub_categoria_novedades on sub_categoria_novedades_id = sub_categoria_novedades.id and categoria_novedades_id = ? inner join roles on roles.id'.$aux.'1 group by sub_categoria_novedades.id',[$request['idCat']]);
        else
          $result = DB::select('select sub_categoria_novedades.id, sub_categoria_novedades.nombre, count(*) as cantidad from sub_categoria_novedades inner join novedades on novedades.categoria_novedades_id=? and novedades.sub_categoria_novedades_id=? and sub_categoria_novedades.id =? inner join roles on roles.id'.$aux.'1 group by sub_categoria_novedades.id',[$request['idCat'],$request['idSubCat'],$request['idSubCat']]);

        foreach ($result as $key => $grafi) $s[] = $grafi;
        return $s; 

    }


    public function getSubCategory(Request $id){

    	$id = $id->all();
    	//$result = DB::select('SELECT sub_categoria_novedades.id, sub_categoria_novedades.nombre AS nombre_subcategoria FROM subcategoria_novedades_has_categoria_novedades INNER JOIN categoria_novedades ON categoria_novedades_id=categoria_novedades.id INNER JOIN sub_categoria_novedades ON subcategoria_novedades_id=sub_categoria_novedades.id where categoria_novedades.id=?',[$id['id']]);
        $result = DB::select('SELECT sub_categoria_novedades.id, sub_categoria_novedades.nombre as nombre_subcategoria FROM categoria_novedades_has_sub_categoria_novedades INNER JOIN categoria_novedades ON categoria_novedades_id=categoria_novedades.id INNER JOIN sub_categoria_novedades ON sub_categoria_novedades_id=sub_categoria_novedades.id where categoria_novedades.id=?',[$id['id']]);
    	foreach ($result as $key => $grafi) $s[] = $grafi;
        return $s;   
    }

    public function getEstadisticasBase(Request $user){

        $user = $user->all();
        //$result = DB::select('select categoria_novedades_id, nombre, count(*) as numeroReportes from novedades inner join categoria_novedades on novedades.categoria_novedades_id = categoria_novedades.id group by categoria_novedades_id'); // anterior consulta sin RolUser
        if($user['user'] != 1) $aux = "="; 
        else $aux = "<>";
        $result = DB::select('select categoria_novedades_id, categoria_novedades.nombre, count(*) as numeroReportes from novedades inner join categoria_novedades on novedades.categoria_novedades_id = categoria_novedades.id 
INNER JOIN roles_has_users on  novedades.users_id = roles_has_users.users_id  and roles_id'.$aux.'1
group by categoria_novedades_id');
        //$result = DB::select('select categoria_novedades_id, nombre, count(*) as numeroReportes from crimenes inner join categoria_novedades on crimenes.categoria_novedades_id = categoria_novedades.id group by categoria_novedades_id');

        foreach ($result as $key => $grafi) $s[] = $grafi;

        $result = DB::select('select count(*) as totalReportes from novedades');
        foreach ($result as $key => $grafi) $t[] = $grafi;
        return array("conteo"=>$s,"total"=>$t);
    }

    public function getAllChart(Request $request){
        return "nada :v";

    }

    public function getDatosCategorias(Request $request){
        $request = $request->all();

        $result = DB::SELECT('select novedades.categoria_novedades_id,categoria_novedades.nombre, municipios.nombre as municipo, sum( novedades.cantidad_edad_0_14) as cantidad0_14 , sum( novedades.cantidad_edad_15_17) as cantidad15_17 ,sum(novedades.cantidad_edad_18_59) as cantidad18_59 ,sum(novedades.cantidad_edad_mayores_60) as cantidadMayores60 from novedades INNER join municipios on municipios.id = novedades.municipios_id inner join categoria_novedades on categoria_novedades.id = novedades.categoria_novedades_id where novedades.municipios_id=? group by categoria_novedades.nombre',[$request['muni']]);
        foreach ($result as $key => $grafi) $s[] = $grafi;
        return $s; // return "none";
    }

}