<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>@yield('title', 'admin') |Admin</title>

    <!-- Bootstrap core CSS -->

    <link rel="stylesheet"  href="{{asset('plugins/css/bootstrap.css')}}">

    <link rel="stylesheet" href="{{asset('vendor/bootstrap/css/bootstrap.css')}}" rel="stylesheet">

    

    <link rel="stylesheet" type="text/css" href="{{asset('css/jquery.easy-pie-chart.css')}}">


    <!-- Custom styles for this template -->
    <link rel="stylesheet" href="{{asset('css/small-business.css')}}" rel="stylesheet">      
    <link rel="stylesheet" href="{{asset('css/bod.css')}}" rel="stylesheet">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.0/css/font-awesome.min.css">

  </head>

		
  <body >
  		
	
			@include('admin.template.partials.nav')

		
		<div style="height: 65px;"></div>
	
		<div class="container" >
			@yield('content')
		</div>
		


		@include('admin.template.partials.footer')
		<script src="{{asset('vendor/jquery/jquery.min.js')}}"></script>
		<script src="{{asset('vendor/jquery/jquery.js')}}"></script>
    	<script src="{{asset('vendor/popper/popper.js')}}"></script>
    	<script src="{{asset('vendor/bootstrap/js/bootstrap.min.js')}}"></script>
    	<script src="{{asset('vendor/bootstrap/js/bootstrap.js')}}"></script>
      <script src="{{asset('js/bod.js')}}"></script>

    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="{{asset('js/mdb.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/control.js')}}"></script>

    <!--pieChartMin-->
    <script type="text/javascript" src="{{asset('js/jquery.easypiechart.js')}}"></script>

    <!-- Canvas A PDF -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.3/jspdf.min.js"></script>


		 
</body>

</html>