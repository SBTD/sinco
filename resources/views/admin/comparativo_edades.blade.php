@extends('admin.template.admin')

@section('title')
        Sincos
@endsection

 
   

@section('content')

  <div class="container" id ="vistaNovedad" data-fruit="3">
        <section class="bg-light">
             <div class="container">
                <div class="row">
                  <div class="col-md-3">
                      <label>Categoria: </label>
                      <select id="categoriaBox">
                        <option value="0" disabled="disabled" selected="selected" > Seleccione </option>
                      </select>
                  </div>

                  <div class="col-md-3">
                    <label>Subcatregoria: </label>
                    <select id="subCategoriaBox">
                      <option value="0" disabled="disabled" selected="selected" > Seleccione </option>
                    </select>
                  </div>

                  <div class="col-md-3">
                      <div class ="row">
                        <div class= "col-md-6"> <label>Gr&aacute;fica: </label></div>
                        <div class= "col-md-6">
                            <div class="radio">
                               <label><input type="radio" value="Barras" name="optradio">Barras</label>
                            </div>
                            <div class="radio">
                               <label><input type="radio" value="Puntos" name="optradio">Puntos</label>
                            </div>
                            <div class="radio">
                                <label><input type="radio" value="Torta" name="optradio">Torta</label>
                            </div>
                            <div class="radio">
                                <label><input type="radio" value="Radar" name="optradio">Radar</label>
                            </div>
                            <div class="radio">
                                <label><input type="radio" value="Polar" name="optradio">Polar</label>
                            </div>
                        </div>
                      </div>
                  </div>


                  <div class="col-md-3"><button class="btn btn-default" name="boton" id="boton">Agregar Gr&aacute;fica</button></div>

                </div>
            </div>
        </section>

        <section class="bg-light">
            <div class="container">
                    <ul class="nav nav-tabs" name="tabsADD" id ="tabsADD">
                      <li class="active"><a data-toggle="tab" href="#home">Home</a></li>
                    </ul>


                <div class="row">              
                          <div class="tab-content col-md-12" id ="tabsDiv" name =  "tabsDiv">
                              <div id="home" class="tab-pane fade in active show">
                                <h3>HOME</h3>                                
                                  <div class="row" id="homeChart">
                                  </div>                                
                                 <canvas id="homeCanvas"></canvas>
                                 <img src="" id="laimagen"/>
                                 <button class="btn btn-primary btn-lg desca" name="desca" data-fruit="homeCanvas" id="desca">Descargar Gr&aacute;fica </button>
                              </div>
                      
                              <div id="menu999999" class="tab-pane fade">
                                <h3>Puntos</h3>
                                <canvas id="barCanvas9999"></canvas>
                              </div>
                          </div>                                
                </div>
            </div>
        </section>      
 </div>

@endsection