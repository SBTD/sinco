@extends('admin.template.admin')

@section('title')
        Sincos
@endsection

 
   

@section('content')


   
  <div class="container" id ="vistaNovedad" data-fruit="4" data-municipio ="001" >
       

        <section class="bg-light">
            <div class="container">
                    <ul class="nav nav-tabs" name="tabsADD" id ="tabsADD">
                      <li class="active"><a data-toggle="tab" href="#home">Home</a></li>
                    </ul>


                <div class="row">              
                          <div class="tab-content col-md-12" id ="tabsDiv" name =  "tabsDiv">
                              <div id="home" class="tab-pane fade in active show">
                                <h3>HOME</h3>                                
                                  <div class="row" id="homeChart">
                                  </div>                                
                                 <canvas id="homeCanvas"></canvas>
                                 <img src="" id="laimagen"/>
                                 <button class="btn btn-primary btn-lg desca" name="desca" data-fruit="homeCanvas" id="desca">Descargar Gr&aacute;fica </button>
                              </div>
                      
                              <div id="menu999999" class="tab-pane fade">
                                <h3>Puntos</h3>
                                <canvas id="barCanvas9999"></canvas>
                              </div>
                          </div>                                
                </div>
            </div>
        </section>      
 </div>

@endsection