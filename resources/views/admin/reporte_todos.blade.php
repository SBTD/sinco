@extends('admin.template.admin')

@section('title')
        Sincos
@endsection

 
   

@section('content')

  <div class="container" id ="vistaNovedad" data-fruit="5" data-municipio ="001" >

  	 <section class="bg-light">
             <div class="container">
                <div class="row">
                  <div class="col-md-4">
                      <label>Categoria: </label>
                      <select id="categoriaBox">
                        <option value="0" disabled="disabled" selected="selected" > Seleccione </option>
                      </select>
                  </div>

                  <div class="col-md-2" hidden="hidden">
                    <label>Subcatregoria: </label>
                    <select id="subCategoriaBox">
                      <option value="0" disabled="disabled" selected="selected" > Seleccione </option>
                    </select>
                  </div>

                  <button class="btn btn-primary col-md-3" name="botonG" id="botonG">Generar Gr&aacute;ficas</button>
                  <button class="btn btn-default col-md-3 " disabled = "disabled" name="descaG" data-fruit="homeCanvas" id="descaG">Descargar Gr&aacute;fica </button>

                </div>
            </div>
        </section>
       

        <section class="bg-light">
            <div class="container">
                <div class="row">              
                          <div class="tab-content col-md-12" id ="tabsDiv" name =  "tabsDiv"> 
                           <div class="row" id="homeChart"></div>                          
                           

                                 <canvas id="homeCanvas"></canvas>
                                 <canvas id="barCanvas"></canvas>
                                 <canvas id="pieCanvas"></canvas>
                                 <canvas id="polarCanvas"></canvas>
                                 <canvas id="lineCanvas"></canvas>
                                 <canvas id="radarCanvas"></canvas>
                                 <img src="" id="laimagen"/>
                                 
                                   
                          </div>                                
                </div>
            </div>
        </section>  

                              <div id="menu999999" class="tab-pane fade">
                                <h3>Puntos</h3>
                                <canvas id="barCanvas9999"></canvas>
                              </div>    
 </div>


@endsection