<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//rutas de usuarios
Route::get('/', function () {
    return view('inicio');
});

Route::get('home', function () {
    return view('inicio');
});

Route::get('paneladmin', function () {
    return view('vadmin');
});


//rutas de inicio home
Route::prefix('inicio')->group(function () {
    Route::get('Conocenos', function () {
    return view('inicio/conocenos');
	});

     Route::get('User', function () {
    return view('inicio/guia_user');
	});

     Route::get('Contact', function () {
    return view('inicio/contact');
	});

     Route::get('Login', function () {
    return view('inicio/login');
	});
});


//rutas del administrador
Route::prefix('paneladmin')->group(function () {
    Route::get('Novedad', function () {
    return view('admin/novedad');
    });

     Route::get('Listado_Novedades', function () {
    return view('admin/listado');
    });

     Route::get('Listado_Municipios', function () {
    return view('admin/municipios');
    });

     Route::get('Listado_Corregimientos', function () {
    return view('admin/corregimientos');
    });
});

Route::prefix('paneladmin/reportes')->group(function () {
    Route::get('Reprt_novedad', function () {
    return view('admin/reporte_novedad');
    });


    // Reporte de novedades realizadas por los ciudadanos por medio del boton de novedad en la pagina de inicio
     Route::get('Reprt_novedad_userr', function () {
    return view('admin/reporte_novedad_userr');
    });

     Route::get('Comparativo_edades', function () {
    return view('admin/comparativo_edades');
    });

     Route::get('Comparativo_municipios', function () {
    return view('admin/comparativo_municipios');
    });

     Route::get('Reporte_todos', function () {
    return view('admin/reporte_todos');
    });
});


Route::prefix('paneladmin/user')->group(function () {
    Route::get('Config_Cuenta', function () {
    return view('admin/config_cuenta');
    });
    Route::get('Config_Sistema', function () {
    return view('admin/config_sistema');
    }); 
});


//no sé que es pero se ve bonito ahí
Auth::routes();


//ruta a los controladores
Route::get('/home', 'HomeController@index')->name('home');
Route::name('categoria')->get('/categoria', 'CategoryController@getCategory');
Route::name('categoria2')->get('/categoria2/{id?}', 'CategoryController@getSubCategory');
Route::name('estadisticasBase')->get('/estadisticasBase/{user?}','CategoryController@getEstadisticasBase');
Route::name('datosCategorias')->get('/datosCategorias/{user?}','CategoryController@getDatosCategorias');
Route::name('conteoGrafica')->get('/conteoGrafica/{user?}','CategoryController@getCountChart');
Route::name('todasGra')->get('/todasGra/{user?}','CategoryController@getAllChart');


